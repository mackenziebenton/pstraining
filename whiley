#!/bin/bash

# number loop
counter=1
while (( counter <= 10 ))
do
  echo $counter
  ((counter=counter+1 ))
  # (( counter+=1 ))
  # counter='expr $counter +1' # This works in sh, bash, ksh
done

# Wait until root has logged in
while ! who | grep ec2-user
do
 echo "ec2-user is not logged in"
 sleep 10
done
echo "ec2-user has logged in"


# count chars per line in a file
# cat /etc/passwd | while read line
cat words | while read line
do
  echo "$line" | wc -c
done

# Alternative slightly faster version
while read line
do
  echo "$line" | wc -c
done <words
